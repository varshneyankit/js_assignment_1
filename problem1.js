// this function accesses and returns the email addresses of all individuals

function getEmails(users) {
  if (Array.isArray(users)) {
    let emails = [];

    for (let index = 0; index < users.length; index++) {
      emails.push(users[index].email);
    }

    return emails;
  } else {
    return [];
  }
}

module.exports = getEmails;
