// this function retrieve and display the first hobby of each individual in the dataset

function getFirstHobbies(users) {
  if (Array.isArray(users)) {
    let firstHobbies = [];

    for (let index = 0; index < users.length; index++) {
      firstHobbies.push(users[index].hobbies[0]);
    }

    return firstHobbies;
  } else {
    return [];
  }
}

module.exports = getFirstHobbies;
