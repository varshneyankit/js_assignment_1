// this function access and print the ages of all individuals in the dataset

function getAges(users) {
  if (Array.isArray(users)) {
    let ages = [];

    for (let index = 0; index < users.length; index++) {
      ages.push(users[index].age);
    }

    return ages;
  } else {
    return [];
  }
}

module.exports = getAges;
