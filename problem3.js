// this function extracts and displays the names of individuals who are students (`isStudent: true`) and live in Australia

function getStudentsByCountry(users, country) {
  if (Array.isArray(users) && typeof country == "string") {
    let students = [];

    for (let index = 0; index < users.length; index++) {
      if (users[index].isStudent && users[index].country == country) {
        students.push(users[index].name);
      }
    }

    return students;
  } else {
    return [];
  }
}

module.exports = getStudentsByCountry;
