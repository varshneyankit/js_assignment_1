// this function retrieves and prints the hobbies of individuals with a specific age, say 30 years old

function getHobbiesByAge(users, age) {
  if (Array.isArray(users) && typeof age == "number") {
    let hobbies = [];

    for (let index = 0; index < users.length; index++) {
      if (users[index].age == age) {
        hobbies.push(users[index].hobbies);
      }
    }

    return hobbies;
  } else {
    return [];
  }
}

module.exports = getHobbiesByAge;
