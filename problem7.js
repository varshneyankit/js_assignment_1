// this function accesses and prints the names and email addresses of individuals aged 25

function getNamesAndEmailsByAge(users, age) {
  if (Array.isArray(users) && typeof age == "number") {
    let resultantUsers = [];

    for (let index = 0; index < users.length; index++) {
      if (users[index].age == age) {
        resultantUsers.push({
          name: users[index].name,
          email: users[index].email,
        });
      }
    }

    return resultantUsers;
  } else {
    return [];
  }
}

module.exports = getNamesAndEmailsByAge;
