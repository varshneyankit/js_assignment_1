const users = require("../data");
const getStudentsByCountry = require("../problem3");

const students = getStudentsByCountry(users, "Australia");

if (students.length != 0) {
  console.log(students);
} else {
  console.log("Data is empty");
}
