const users = require("../data");
const getEmails = require("../problem1");

const emails = getEmails(users);

if (emails.length != 0) {
  console.log(emails);
} else {
  console.log("Data is empty");
}
