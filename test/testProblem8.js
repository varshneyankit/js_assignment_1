const users = require("../data");
const getCitiesAndCountries = require("../problem8");

const citiesAndCountries = getCitiesAndCountries(users);

if (citiesAndCountries.length != 0) {
  console.log(citiesAndCountries);
} else {
  console.log("Data is empty");
}
