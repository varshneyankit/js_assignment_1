const users = require("../data");
const getAges = require("../problem5");

const ages = getAges(users);

if (ages.length != 0) {
  console.log(ages);
} else {
  console.log("Data is empty");
}
