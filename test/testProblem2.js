const users = require("../data");
const getHobbiesByAge = require("../problem2");

const hobbies = getHobbiesByAge(users, 30);

if (hobbies.length != 0) {
  console.log(hobbies);
} else {
  console.log("Data is empty");
}
