const users = require("../data");
const getUserByIndex = require("../problem4");

const user = getUserByIndex(users, 3);

if (user != null) {
  console.log(user);
} else {
  console.log("User not found");
}
