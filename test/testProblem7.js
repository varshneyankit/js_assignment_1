const users = require("../data");
const getNamesAndEmailsByAge = require("../problem7");

const resultantUsers = getNamesAndEmailsByAge(users, 25);

if (resultantUsers.length != 0) {
  console.log(resultantUsers);
} else {
  console.log("Data is empty");
}
