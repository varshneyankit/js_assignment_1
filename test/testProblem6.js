const users = require("../data");
const getFirstHobbies = require("../problem6");

const firstHobbies = getFirstHobbies(users);

if (firstHobbies.length != 0) {
  console.log(firstHobbies);
} else {
  console.log("Data is empty");
}
