// this function access and log the city and country of each individual in the dataset

function getCitiesAndCountries(users) {
  if (Array.isArray(users)) {
    let citiesAndCountries = [];

    for (let index = 0; index < users.length; index++) {
      citiesAndCountries.push({
        city: users[index].city,
        country: users[index].country,
      });
    }

    return citiesAndCountries;
  } else {
    return [];
  }
}

module.exports = getCitiesAndCountries;
