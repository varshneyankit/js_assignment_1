// this function accesses and logs the name and city of the individual at the index position 3 in the dataset

function getUserByIndex(users, index) {
  if (Array.isArray(users) && typeof index == "number") {
    let userData = null;

    if (index >= 0 && index < users.length) {
      userData = {
        name: users[index].name,
        city: users[index].city,
      };
    }

    return userData;
  } else {
    return null;
  }
}

module.exports = getUserByIndex;
